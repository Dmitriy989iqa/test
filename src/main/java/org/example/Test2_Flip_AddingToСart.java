package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;


public class Test2_Flip_AddingToСart {

    public static void main(String[] args) {

        // Устанавливаем путь к драйверу Chrome
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");

        //Создаем настройки WebDriver для Chrome
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        WebDriver driver = new ChromeDriver(options);

        // Устанавливаем явное ожидание в 10 секунд
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        try {
            // Открываем страницу b13shop
            driver.get("https://www.flip.kz/user?personalis");

            // Ожидание, пока появится ввод поля "логин"
            WebElement UserField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='username']")));
            UserField.sendKeys("testwebsite989@gmail.com");

            // Ожидание, пока появится ввод поля "пароль"
            WebElement PasswordField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='password'][1]")));
            PasswordField.sendKeys("98900");

            // Ожидание, пока кнопка "Войти" не станет кликабельной
            WebElement  JoinButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Войти']")));
            JoinButton.click();

            // Ожидание, пока появится ввод поля "Поиск товара"
            WebElement SearchField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='search']")));
            SearchField.sendKeys("пылесос");

            // Ожидание, пока кнопка "Найти" не станет кликабельной
            WebElement SearchButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Найти']")));
            SearchButton.click();

            // Ожидание, пока кнопка "Товара" не станет кликабельной
            WebElement ProductButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@class='pic l-h-250'][1]")));
            ProductButton.click();

            // Ожидание, пока кнопка "Добавить в корзину" не станет кликабельной
            WebElement AddCartButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='cart_button']")));
            AddCartButton.click();

            // Ожидание, пока кнопка "Корзина" не станет кликабельной
            WebElement GoToCartButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@class='gray nbtn float-left']")));
            GoToCartButton.click();
            // Проверяем, наличие товара в корзине
            WebElement ChekboxButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='selected[]']")));
            ChekboxButton.click();

            // Вывод сообщения об успешной/неуспешной отправки письма
            System.out.println("Товар успешно добавлен в корзину!");

        } catch (Exception e) {
            System.out.println("Ошибка: " + e.getMessage());
        } finally {
            try { //Ожидание закрытие браузера
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            driver.quit();
        }
    }
}
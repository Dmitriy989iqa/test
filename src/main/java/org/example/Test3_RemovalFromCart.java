package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;


public class Test3_RemovalFromCart {

    public static void main(String[] args) {

        // Устанавливаем путь к драйверу Chrome
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");

        //Создаем настройки WebDriver для Chrome
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        WebDriver driver = new ChromeDriver(options);

        // Устанавливаем явное ожидание в 10 секунд
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        try {
            // Открываем страницу b13shop
            driver.get("https://www.flip.kz/user?personalis");

            // Ожидание, пока появится ввод поля "логин"
            WebElement UserField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='username']")));
            UserField.sendKeys("testwebsite989@gmail.com");

            // Ожидание, пока появится ввод поля "пароль"
            WebElement PasswordField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='password'][1]")));
            PasswordField.sendKeys("98900");

            // Ожидание, пока кнопка "Войти" не станет кликабельной
            WebElement JoinButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Войти']")));
            JoinButton.click();

            // Ожидание, пока кнопка "Корзина" не станет кликабельной
            WebElement CartButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='cart cell']")));
            CartButton.click();

            // Ожидание, пока кнопка "Удалить" не станет кликабельной
            WebElement DeleteButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@class='nbtn gray small m-r-10']")));
            DeleteButton.click();

            // Вывод сообщения об успешной/неуспешной отправки письма
            System.out.println("Удаление товара прошло успешно!");

        } catch (Exception e) {
            System.out.println("Ошибка: " + e.getMessage());
        } finally {
            try { //Ожидание закрытие браузера
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
           driver.quit();
        }
    }
}